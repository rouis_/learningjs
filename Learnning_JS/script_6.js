"use strict";

const listeCodons = [
    { name: "UCU", acideAmine: "Sérine"},
    { name: "UCC", acideAmine: "Sérine"},
    { name: "UCA", acideAmine: "Sérine"},
    { name: "UCG", acideAmine: "Sérine"},
    { name: "AGU", acideAmine: "Sérine"},
    { name: "AGC", acideAmine: "Sérine"},
    { name: "CCU", acideAmine: "Proline"},
    { name: "CCC", acideAmine: "Proline"},
    { name: "CCA", acideAmine: "Proline"},
    { name: "CCG", acideAmine: "Proline"},
    { name: "UUA", acideAmine: "Leucine"},
    { name: "UUG", acideAmine: "Leucine"},
    { name: "UUU", acideAmine: "Phénylalanine"},
    { name: "UUC", acideAmine: "Phénylalanine"},
    { name: "CGU", acideAmine: "Arginine"},
    { name: "CGC", acideAmine: "Arginine"},
    { name: "CGA", acideAmine: "Arginine"},
    { name: "CGG", acideAmine: "Arginine"},
    { name: "AGA", acideAmine: "Arginine"},
    { name: "AGG", acideAmine: "Arginine"},
    { name: "UAU", acideAmine: "Tyrosine"},
    { name: "UAC", acideAmine: "Tyrosine"}
];

function decoupeEnCodons(arn, size) {
    let codons = [];
    while (arn.length > 0) {
        codons.push(arn.substring(0, size));
        arn = arn.substring(size, arn.length);
    }

    return codons;
}

function trouverAcideAmine(listeCodons, name) {
    let res;

    listeCodons.forEach(listeCodon => {
        if (name.localeCompare(listeCodon.name) == 0) res = listeCodon.acideAmine;
    });

    return res;
}

function proteine(listeCodons, arn) {
    let proteine;
    let codons = decoupeEnCodons(arn, 3);

    codons.forEach(codon => {
        !proteine ? proteine = trouverAcideAmine(listeCodons, codon) : proteine += ("-" + trouverAcideAmine(listeCodons, codon));
    });

    return proteine;
}

function script6() {
    let arn = "CCUCGCCGGUACUUCUCG";
    let res = proteine(listeCodons, arn);

    console.log(res);
}

script6();