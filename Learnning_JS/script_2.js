"use strict";

function factorielle(nbr) {
    let res = 1;
    let i = 1;

    while (i <= nbr) {
        res *= i;
        i++;
    }

    return res;
}

function script2() {
    let nbr = prompt("De quel nombre veux-tu calculer la factorielle ? ");

    console.log("Le résultat est : " + factorielle(nbr));
}

script2();