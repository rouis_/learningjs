"use strict";

const books = [
    { title: 'Gatsby le magnifique', id: 133712, rented: 39 },
    { title: 'A la recherche du temps,perdu', id: 237634, rented: 28 },
    { title: 'Orgueil & Préjugés', id: 873495, rented: 67 },
    { title: 'Les frères Karamazov', id: 450911, rented: 55 },
    { title: 'Dans les forêts de Sibérie', id: 8376365, rented: 15 },
    { title: 'Pourquoi j\'ai mangé mon père', id: 450911, rented: 45 },
    { title: 'Et on tuera tous les affreux', id: 67565, rented: 36 },
    { title: 'Le meilleur des mondes', id: 88847, rented: 58 },
    { title: 'La disparition', id: 364445, rented: 33 },
    { title: 'La lune seule le sait', id: 63541, rented: 43 },
    { title: 'Voyage au centre de la Terre', id: 4656388, rented: 38 },
    { title: 'Guerre et Paix', id: 748147, rented: 19 }
];

function tousEmpruntes(books) {
    let notRented = 0;
    let res = false;

    books.forEach(book => {
        if (book.rented <= 0) notRented++;
    });
    notRented > 0 ? res = false : res = true;

    return res;
}

function plusEmprunte(books) {
    let res = books[0];

    books.forEach(book => {
        if (book.rented > res.rented) res = book;
    });

    return res;
}

function moinsEmprunte(books) {
    let res = books[0];

    books.forEach(book => {
        if (book.rented < res.rented) res = book;
    });

    return res;
}

function trouverLivreId(books, id) {
    let res;

    books.forEach(book => {
        if (book.id == id) res = book;
    });

    return res;
}

function supprimerLivreId(books, id) {
    let res = 0;

    books.forEach(book => {
        if (book.id == id) {
            let index = books.indexOf(book);

            if (index > -1) {
                books.splice(index, 1);
            }
            ++res;
        }
    });

    return res;
}

function trierLivre(books) {
    let newArray = Array();

    books.forEach(book => {
        newArray.push(book);
    });
    newArray.sort((a, b) => a.title.localeCompare(b.title));

    return newArray;
}

function script5() {
    let a = tousEmpruntes(books);
    let b = plusEmprunte(books);
    let c = moinsEmprunte(books);
    let d = trouverLivreId(books, 873495);
    let e = supprimerLivreId(books, 133712);;
    let f = trierLivre(books);

    console.log("Est-ce que tous les livres ont été au moins empruntés une fois ?");
    console.log(a);
    console.log("Quel est livre le plus emprunté ?");
    console.log(b);
    console.log("Quel est le livre le moins emprunté ?");
    console.log(c);
    console.log("Trouve le livre avec l'ID: `873495` ;");
    console.log(d);
    console.log("Supprime le livre avec l'ID: `133712` ;");
    console.log(e);
    console.log("Trie les livres par ordre alphabétique (sans celui avec l'ID `133712` car il est supprimé).");
    console.log(f);
}

script5();