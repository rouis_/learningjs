"use strict";

const entrepreneurs = [
    { first: 'Steve', last: 'Jobs', year: 1955 },
    { first: 'Oprah', last: 'Winfrey', year: 1954 },
    { first: 'Bill', last: 'Gates', year: 1955 },
    { first: 'Sheryl', last: 'Sandberg', year: 1969 },
    { first: 'Mark', last: 'Zuckerberg', year: 1984 },
    { first: 'Beyonce', last: 'Knowles', year: 1981 },
    { first: 'Jeff', last: 'Bezos', year: 1964 },
    { first: 'Diane', last: 'Hendricks', year: 1947 },
    { first: 'Elon', last: 'Musk', year: 1971 },
    { first: 'Marissa', last: 'Mayer', year: 1975 },
    { first: 'Walt', last: 'Disney', year: 1901 },
    { first: 'Larry', last: 'Page', year: 1973 },
    { first: 'Jack', last: 'Dorsey', year: 1976 },
    { first: 'Evan', last: 'Spiegel', year: 1990 },
    { first: 'Brian', last: 'Chesky', year: 1981 },
    { first: 'Travis', last: 'Kalanick', year: 1976 },
    { first: 'Marc', last: 'Andreessen', year: 1971 },
    { first: 'Peter', last: 'Thiel', year: 1967 }
];

function filtreParAnnee(entrepreneurs) {
    let year = 1970;
    let decade = 10;
    let newArray = Array();

    entrepreneurs.forEach(entrepreneur => {
        if (entrepreneur.year >= year && entrepreneur.year < (year + decade)) {
            newArray.push(entrepreneur);
        }
    });

    return newArray;
}

function prenomNom(entrepreneurs) {
    let newArray = Array();

    entrepreneurs.forEach(entrepreneur => {
        newArray.push({ first: entrepreneur.first, last: entrepreneur.last });
    });

    return newArray;
}

function ageAujourdhui(entrepreneurs) {
    let year = 2022;
    let newArray = Array();

    entrepreneurs.forEach(entrepreneur => {
        newArray.push({ first: entrepreneur.first, last: entrepreneur.last, age: year - entrepreneur.year });
    });

    return newArray;
}

function triNom(entrepreneurs) {
    let newArray = Array();

    entrepreneurs.forEach(entrepreneur => {
        newArray.push(entrepreneur);
    });
    newArray.sort((a, b) => a.last.localeCompare(b.last));

    return newArray;
}

function script4() {
    let a = filtreParAnnee(entrepreneurs);
    let b = prenomNom(entrepreneurs);
    let c = ageAujourdhui(entrepreneurs);
    let d = triNom(entrepreneurs);

    console.log("Filtre dans cette liste les entrepreneurs qui sont nés dans les années 70 ;");
    console.log(a);
    console.log("Sors une array qui contient le prénom et le nom des entrepreneurs ;");
    console.log(b);
    console.log("Quel âge aurait chaque inventeur aujourd'hui ?");
    console.log(c);
    console.log("Trie les entrepreneurs par ordre alphabétique du nom de famille.");
    console.log(d);
}

script4();