"use strict";

function decoupeQuestion(question) {
    let lastChar;
    let nbCaps = 0;
    let nbNotCaps = 0;

    if (!question) return 3;
    else {
        if (question[question.length - 1]) lastChar = question[question.length - 1];
        if (lastChar == '?') return 0;
        for (let i = 0; i < question.length; i++) {
            if (question[i].match(/^[a-z]+$/)) nbNotCaps++;
            if (question[i].match(/^[A-Z]+$/)) nbCaps++;
        }
        if (nbCaps > 0 && nbNotCaps == 0) return 1;
        if (question.toLocaleLowerCase().indexOf("fortnite") != -1) return 2;    
    }

    return -1;
}

function reponseAdaptee(typeDeQuestion) {
    switch (typeDeQuestion) {
        case 0:
          console.log("Ouais Ouais...");
          break;
        case 1:
          console.log("Pwa, calme-toi...");
          break;
        case 2:
          console.log("on s' fait une partie soum-soum ?");
          break;
        case 3:
          console.log("t'es en PLS ?");
          break;
        default:
          console.log("balek.");
      }
}

function chatBot() {
    let question = prompt("Entrez votre question à Acné-bot : ");

    reponseAdaptee(decoupeQuestion(question));
}

function script7() {
    chatBot();
}

script7();