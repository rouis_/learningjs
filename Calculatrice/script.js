"use strict";

let nbrA = document.getElementById("nbrA");
let nbrB = document.getElementById("nbrB");
let operation = document.getElementById("operation");
let boutonCalcul = document.getElementById("btnCalculer");
let calculatrice = document.getElementById("calculatrice");
let lienAfficher = document.getElementById("lienAfficher");
let lienCacher = document.getElementById("lienCacher");
let infoZone = document.getElementById("infoZone");
let infoText = document.getElementById("infoText");

boutonCalcul.addEventListener("click", (event) => {
    let a = Number(nbrA.value);
    let b = Number(nbrB.value);
    let result = 0;

    event.preventDefault();
    if (operation.value == "+") result = a + b;
    else if (operation.value == "-") result = a - b;
    else if (operation.value == "*") result = a * b;
    else if (operation.value == "/") result = a / b;
    else result = 0;
    alert(a + operation.value + b + "=" + result);
});

lienAfficher.addEventListener("click", () => {
    calculatrice.classList.remove("hide");
});

lienCacher.addEventListener("click", () => {
    calculatrice.classList.add("hide");
});

nbrA.addEventListener("mouseover", () => {
    infoText.innerHTML = "Saisir un chiffre";
});

nbrA.addEventListener("mouseout",  () => {
    infoText.innerHTML = "";
});

nbrB.addEventListener("mouseover", () => {
    infoText.innerHTML = "Saisir un chiffre";
});

nbrB.addEventListener("mouseout",  () => {
    infoText.innerHTML = "";
});

operation.addEventListener("mouseover", () => {
    infoText.innerHTML = "Selectionner un operateur: + - * /";
});

operation.addEventListener("mouseout",  () => {
    infoText.innerHTML = "";
});

boutonCalcul.addEventListener("mouseover", () => {
    infoText.innerHTML = "Effectuer le calcul";
});

boutonCalcul.addEventListener("mouseout",  () => {
    infoText.innerHTML = "";
});